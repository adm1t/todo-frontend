
export default {
  mode: 'universal',
  /*
   ** Server options
   */
  server: {
    port: 8080,
    host: "0.0.0.0",
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'TODO Agency',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Branding & digital agency' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800&display=swap&subset=cyrillic' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    '~/node_modules/normalize.css/normalize.css',
    '~/assets/styles/main.scss',
    '~/node_modules/swiper/dist/css/swiper.css'
  ],
  styleResources: {
    scss: [
      '~/assets/styles/_core.scss'
    ]
  },
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/vue-awesome-swiper', ssr: false }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/style-resources'
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
