class ScrollStatus {
  constructor(el) {
    this.target = el;
  }

  get progress() {
    const { bottom, height } = this.target.getBoundingClientRect();
    let progress = 1 - (bottom / (window.innerHeight * (height / window.innerHeight + 1)));

    if (progress >= 1) progress = 1;
    if (progress <= 0) progress = 0;

    return progress;
  }

  get enterProgress() {
    const { top } = this.target.getBoundingClientRect();
    let progress = 1 - top / window.innerHeight;

    if (progress >= 1) progress = 1;
    if (progress <= 0) progress = 0;

    return progress;
  }

  get leaveProgress() {
    const { bottom } = this.target.getBoundingClientRect();
    let progress = 1 - bottom / window.innerHeight;

    if (progress >= 1) progress = 1;
    if (progress <= 0) progress = 0;

    return progress;
  }

  get isView(){
    return this.progress > 0 && this.progress < 1
  }
}

export default ScrollStatus;
