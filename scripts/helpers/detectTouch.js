function detectTouch() {
  if (typeof window === 'undefined') return;
  return 'ontouchstart' in window || navigator.maxTouchPoints;
}

const isTouch = detectTouch();
export default isTouch;
