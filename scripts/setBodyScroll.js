import scrollWidth from '~/scripts/helpers/scrollWidth';

const NO_SCROLL = 'no-scroll';

export function preventScroll() {
  if (typeof document === 'undefined') return;

  document.body.classList.add(NO_SCROLL);
  document.body.style.marginRight = scrollWidth + 'px';
}

export function allowScroll() {
  if (typeof document === 'undefined') return;

  document.body.classList.remove(NO_SCROLL);
  document.body.style.marginRight = '';
}
