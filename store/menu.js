export const state = () => ({
  isOpen: false,
  list: [
    { title: 'Проекты',  to: '/' },
    { title: 'Агенство', to: '/about' },
    { title: 'Контакты', to: '/contacts' }
  ]
});

export const mutations = {
  toggle(state) {
    state.isOpen = !state.isOpen;
  },
  open(state) {
    state.isOpen = true
  },
  close(state) {
    state.isOpen = false
  }
};
