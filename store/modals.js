export const state = () => ({
  components: {
    order: { isOpen: false },
  },
  addOverlay: true,
});

export const mutations = {
  toggle({ components }, name) {
    const modal = components[name];
    modal.isOpen = !modal.isOpen;
  },
  open({ components }, name) {
    const modal = components[name];
    modal.isOpen = true;
  },
  close({ components }, name) {
    const modal = components[name];
    modal.isOpen = false;
  },
  closeAll({ components }) {
    const modals = Object.values(components);
    modals.forEach(modal => {
      modal.isOpen = false;
    });
  },
  disableOverlay(state) {
    state.addOverlay = false;
  },
  allowOverlay(state) {
    state.addOverlay = true;
  },
};

export const getters = {
  openedModal({ components }) {
    const modals = Object.values(components);
    return !!modals.filter(modal => modal.isOpen).length;
  },
};
