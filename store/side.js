export const state = () => ({
  components: {
    leftTop:     { is: '', side: 'left',  pos: 'top',    contrast: false },
    leftCenter:  { is: '', side: 'left',  pos: 'center', contrast: false },
    leftBottom:  { is: '', side: 'left',  pos: 'bottom', contrast: false },
    rightTop:    { is: '', side: 'right', pos: 'top',    contrast: false },
    rightCenter: { is: '', side: 'right', pos: 'center', contrast: false },
    rightBottom: { is: '', side: 'right', pos: 'bottom', contrast: false },
  },
  availableComponents: [
    '',
    'Logo',
    'Burger',
    'Scrolling',
    'Mail',
    'Pagination',
    'OrderSocialBtn',
    'lang'
  ]
});

export const mutations = {
  setComponentProperty(state, params) {
    let { key, prop, val } = params;
    if(state.components[key][prop] !== val) {
      state.components = {
        ...state.components,
        [key]: {
          ...state.components[key],
          [prop] : val
        }
      }
    }
  },
  setComponents(state, newComponents) {
    for (let key in newComponents) {
      let componentIsAvailable = state.availableComponents.findIndex(el => el === newComponents[key]) > -1;
      if (componentIsAvailable && state.components.hasOwnProperty(key)) {
        state.components[key].is = newComponents[key];
      }
    }
  }
};

export const actions = {
  defineContrastWithScroll(ctx, scrollStatus) {
    let { isView, enterProgress, leaveProgress } = scrollStatus;

    for (let key of Object.keys(ctx.state.components)) {
      const item = ctx.state.components[key];
      ctx.commit('setComponentProperty', {
        key,
        prop: 'contrast',
        val: (item.pos === 'top'    && enterProgress > 0.95 && leaveProgress < 0.95)
          || (item.pos === 'center' && enterProgress > 0.49 && leaveProgress < 0.49)
          || (item.pos === 'bottom' && enterProgress > 0.02 && leaveProgress < 0.02)
      });
    }
  },
  setContrast(ctx, bool) {
    for (let key of Object.keys(ctx.state.components)) {
      ctx.commit('setComponentProperty', {
        key,
        prop: 'contrast',
        val: bool
      });
    }
  },
  setComponent({ commit }, { key, val, prop }) {
    commit('setComponentProperty', { key, prop, val });
  },
  setComponents({ commit }, newComponents) {
    commit('setComponents', newComponents);
  }
};

export const getters = {
  components(state) {
    let components = {};
    for (let key of Object.keys(state.components)) {
      components[key] = state.components[key].is;
    }
    return components;
  }
}
